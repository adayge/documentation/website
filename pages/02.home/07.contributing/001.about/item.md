---
title: About Adayge Documentation
date: December 13, 2020
taxonomy:
    category: [Help]
    tag: []
    author: DaveLoper
---

# Adayge Documentation
## Welcome

Welcome to the **Adayge documentation** site. Thank you for your interest and participation in our community. Here you will find a library of useful documents to help you deploy Adayge applications, microservices and microservice platforms that will help you run your systems in a scalable, easy to manage, and secure way.

## Navigation

Documentation on this site primarily focuses on three main points; Adayge applications/microservices, 3rd-party microservices, and Platforms. Additionally, you will find help here for making contributions to this documentation.

## Site Policies

Content in the site is copyright 2020-2021 under the [CC-BY-SA license](https://creativecommons.org/licenses/by-sa/2.0/).

## Contributors

If you would like to contribute documentation or improvements to this site, please create a merge request on gitlab where the [documentation is maintained](https://gitlab.com/adayge/documentation/website).

# Links
 * [This page discussion](https://forums.adayge.com/t/about-adayge-documentation/14)
