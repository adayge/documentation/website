---
title: 'Contributing to Adayge Documentation'
date: '13-12-2020 00:00'
taxonomy:
    category:
        - Help
    author:
        - DaveLoper
visible: false
---

The adaYge docmentation uses Markdown in order to provide pages. Markdown allows for seamless context between our git project for documentation and the website. You can even checkout the entire documentation project and work on your own local fork. That way, if you see any improvement that you would like to suggest, you can simply modify your local copy and make a git merge request.

To get started with creating content for this site, please visit the [content creator's](content) section.

To learn more about the syntax of this site, please visit the [typography page](typography).

# Links
 * [This page discussion](https://forums.adayge.com/t/contributing-to-adayge-documentation/16)
