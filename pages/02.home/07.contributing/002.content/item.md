---
title: Content and Copyright
date: December 13, 2020
taxonomy:
    category: [Help]
    tag: []
    author: DaveLoper
---

# Content Creation
adaYge documentation is free to use and you can edit and create contributions. Content on this site is copyleft under the CC-BY-SA license. All contributions become owned by Adayge. Because we use 'git', all contributions are logged in the system approved by the merges. Major content contributors receive recognition beyond the commit log and may be invited to join our community and team at various different levels.

# Process
Whether you are an admin, community member, or someone just passing by you can modify the site information using the following process:

 * Fork the documentation website
 * Make changes locally
 * Make a merge request in Gitlab
 * Your changes are approved or rejected, you will be notified via Gitlab
 * For new pages, a topic will be create or used from the forums to track the page
 * Links to the page you created will be made in the forum post associated with the page
 * A link to the forum will be added to the page

# git
Adayge uses 'git' for this documentation site. You can visit the site here:

[https://gitlab.com/adayge/documentation/website]

If you have git on your system, you can copy the full documentation site with the following:

```
git clone https://gitlab.com/adayge/documentation/website.git
```

You can also fork the full project to your own git repo, make changes and then make a git merge request.

## Installing and Using git on Windows

## Installing and Using git on Mac

## Installing and Using git on Linux

# Links
 * [This page discussion: About Adayge Documentation](https://forums.adayge.com/t/content-and-copyright/15)
