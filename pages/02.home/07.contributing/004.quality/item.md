---
title: Quality and Reliability
date: December 25, 2020
taxonomy:
    category: [Help]
    tag: []
    author: DaveLoper
---
Microservices and platforms are rated on a scale of zero to 10 (0-10) by Adayge support personal. The following constitutes criteria for which this is judged:

 * Quality
   * The general utility of the offering (0-4 points)
   * The bugginess of the offering and the amount of support (0-3 points)
   * Quality of packaging from upstream (0-3 points)
     * One of the points is given if the package is distributed by the producer
 * Reliability
   * Timeliness of updates from upstream (0-4 points)
   * Amount of participation in the Adayge Community (0-3 points)
   * The simplicity of the deployment (0-3 points)

# Links
 * [This page discussion]()
