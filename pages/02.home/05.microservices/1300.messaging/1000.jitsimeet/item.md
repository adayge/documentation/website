---
title: Jitsi Meet
date: December 19, 2020
taxonomy:
    category: [Microservices]
    tag: [microservice, docker, podman]
    author: DaveLoper
---

# Jitsi Meet
Jitsi Meet is free and open source video collaboration suite similar to Zoom or GotoMeeting. Some of the features include:

 * Enterprise Ready
 * End to End Encryption
 * Private transmit data
 * Private meta data
 * Max participants
 * Android/IOS Support
 * Auto active speaker
 * Text Chat
 * Password Lock Rooms
 * Screen Share
 * Stream to Youtube
 * Shared text document
 * Raise/Lower Hand
 * Participant stats
 * Push to Talk Support
 * Broadcast Youtube
 * Audio Only Mode

adaYge supports Jitsi Meet through this documentation, its forums, installation services, and as a managed service.

Reliability Score: **10/10**
Quality Score: **10/10**

# Installation
As a microservice, Jitsi Meet is easy to configure and deploy. You will need a compatible platform to run Jitsi Meet. The codebase is maintainted by the open source project. This earns it a higher trust and higher quality.

## Platform Prerequisite
The Jitsi Meet microservice runs as a container and is compatible with Docker and Podman.

Recommended Platform:
 * Proxmox
 * Docker
 * Portainer

Prerequisite Packages:
 * git

## Deployment
Run the following commands in order to deploy Jitsi Meet on your platform:

```
mkdir -p /var/lib/adayge/containers/
cd /var/lib/adayge/containers
git clone https://gitlab.com/adayge/openapps/jitsimeet.git
chmod 700 /var/lib/adayge/containers/jitsimeet/deploy/init
```

# Support

## Backup

## Restore


# Links
 * [This page discussion]()
 * [CentOS deployment](https://blog.ssdnodes.com/blog/install-jitsi-meet-with-centos-docker-tutorial/)
 * [.env Parameters](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker)
 * [Another Guide](https://www.scaleway.com/en/docs/deploy-jitsi-meet-with-docker/)
