---
title: Postfix Relay
date: January 5, 2021
taxonomy:
    category: [Microservices]
    tag: [microservice, docker, podman]
    author: DaveLoper
---

# Postfix Relay
Postfix is a mail server which transports mail. It provides SMTP services which enables the sending of email. This service does not provide a mechanism for receiving email.

Ideally, you can use this service to overcome barriers that may exist when your ISP blocks the sending of email or if your devices that need to send email do not have mechanism for sending mail. Examples of this are:

 * Scanners and fax machines that have a send email function
 * Scripts that have send email abilities but don't have their own email server or are being blocked

The flow of data is basically

1. Application sends SMTP message to our 'Postfix Relay'
2. 'Postfix Relay' intakes mail on specified port
3. 'Postfix Relay' opens SMTPS submission to the specified mail server.
  1. Logs in with specified Username and Password
  2. Sends email
  3. Closes session
4. 'Postfix Relay' closes connection, logs results.


# Installation
Installation and setup in Adayge's framework is simple and straight forward

## Platform Prerequisite
The Postfix Relay microservice runs as a container and is compatible with Docker and Podman.

Recommended Platform:
 * Proxmox
 * Docker
 * Portainer

Prerequisite Packages:
 * git

## Deployment
Run the following commands in order to deploy Postfix Relay on your platform:

```
mkdir -p /var/lib/adayge/containers/
cd /var/lib/adayge/containers

```

# Support

## Backup

## Restore


# Links
 * [This page discussion]()
 * [CentOS deployment](https://blog.ssdnodes.com/blog/install-jitsi-meet-with-centos-docker-tutorial/)
 * [.env Parameters](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker)
 * [Another Guide](https://www.scaleway.com/en/docs/deploy-jitsi-meet-with-docker/)
