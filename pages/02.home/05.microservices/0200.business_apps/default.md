---
title: Business Apps
date: March 2, 2020
taxonomy:
    category: [Microservices]
    tag: [microservice]
    author: DaveLoper
---

# Business Apps
Business apps supported by Adayge include:

 * Accounting
 * Line of Business
 * Productivity
 * Scheduling
   * [OpenMAINT](openmaint)
