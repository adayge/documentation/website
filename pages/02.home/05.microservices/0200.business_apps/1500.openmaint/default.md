---
title: OpenMAINT
date: March 2, 2020
taxonomy:
    category: [Microservices]
    tag: [microservice, container]
    author: DaveLoper
---

# OpenMAINT
OpenMAINT is a web-based application for maintaining your stuff. Whether vehicles, fire extinguishers, batteries in smoke detectors, building, or computers, OpenMAINT can help you keep schedules and increase responsibility in staff responsible for maintaining your infrastructure. OpenMAINT is self-styled as a 'Computerized Maintenance Management System'.

adaYge supports OpenMAINT as a suite of microservices. You will need a container platform capable of running OpenMAINT. adaYge Professional Services can help you configure and maintain your container platform, install and maintain OpenMAINT, backup and restore your database, and provide infrastructure support for both open source and paid versions of OpenMAINT.

# Installation
Use the adaYge deployment script to deploy OpenMAINT. The current microservice deployment is based off of the work of [rsilva4](https://github.com/rsilva4/docker-openmaint).

# Support
The following are useful commands for manual backup and restore of the OpenMAINT database. NOTE: Current version is v1.1

The default password is 'test'.

## Backup
To backup the database

```
pg_dump -h localhost -p 5555 -U postgres cmdbuild > /var/lib/adayge/containers/openmaint/backup/root/openmaint-20201212.psql
```

## Restore
Before restoring the database, you should make sure to have a good and/or recent backup. Next, you will need to drop the current database and re-create it. If you do not, there will be problem importing a new database on top of the old one.

```
dropdb -h localhost -p 5555 -U postgres cmdbuild
createdb -h localhost -p 5555 -U postgres cmdbuild
```
**Choose one**

 * If the database is in binary format (ie. restoring a blank or demo database from the installation set):

```
pg_restore -h localhost -p 5555 -U postgres --dbname=cmdbuild --verbose /root/empty.dump
```

 * If the database is in raw format, you will need to use psql instead (ie. a recovery of the data in your backups):

```
psql -h localhost -p 5555 -U postgres cmdbuild < /root/test_db_openmaint.psql
```


## Download resources file

An example of a download for a resource file for version 2.1 (not this version):

```
lynx https://sourceforge.net/projects/openmaint/files/2.1/openmaint-2.1-3.3-b-resources.zip/download
```
