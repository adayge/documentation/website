---
title: Clam Antivirus
date: December 25, 2020
taxonomy:
    category: [Microservices]
    tag: [microservice, docker, podman]
    author: DaveLoper
---

# Clam Antivirus
Clam Antivirus is a community driven project. It is versatile in its ability to scan email, gateway proxies, and file systems. As a container, it can be tied to a handoff approach with tools like Amavis, Dansguardian, and others. In addition, the engine is compatible with other virus pattern matching. For example, you can pass content through both the Clam Signature database as well as Kaspersky or other antivirus signatures. Best of all, Clam AV is open source.

adaYge supports ClamAV through this documentation, its forums, installation services, and as a managed service.

Quality Score: **9/10** (4/3/2)
Reliability Score: **8/10** (4/1/3)

# Installation
As a microservice, ClamAV is easy to configure and deploy. You will need a compatible platform to run ClamAV. The codebase is maintainted by the open source project. This earns it a higher trust and higher quality.

## Platform Prerequisite
The ClamAV microservice runs as a container and is compatible with Docker and Podman.

Recommended Platform:
 * Proxmox
 * Docker
 * Portainer

Prerequisite Packages:
 * git

## Deployment
Run the following commands in order to deploy Jitsi Meet on your platform:

```
mkdir -p /var/lib/adayge/containers/
cd /var/lib/adayge/containers
git clone https://gitlab.com/adayge/openapps/clamav.git
chmod 700 /var/lib/adayge/containers/clamav/deploy/init
/var/lib/adayge/containers/clamav/deploy/init
```

# Support

## Backup

## Restore


# Links
 * [This page discussion]()
