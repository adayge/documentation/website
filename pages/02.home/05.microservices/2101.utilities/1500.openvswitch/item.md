---
title: OpenVSwitch
date: December 31, 2020
taxonomy:
    category: [Microservices]
    tag: [microservice, virtualmachine]
    author: DaveLoper
---

# OpenVSwitch


# Installation

## Platform Prerequisite

Recommended Platform:
 * Ubuntu
 * Debian
 * CentOS
 * Proxmox

Prerequisite Packages:
 * x

## Deployment

# Support

## Backup

## Restore


# Links
 * [This page discussion]()
 * [Basic Configuration Guide](https://docs.openvswitch.org/en/latest/faq/configuration/)
 * [os-net-config](https://alesnosek.com/blog/2015/09/28/network-configuration-with-os-net-config/)
 * [Redhat Guide](https://developers.redhat.com/blog/2018/11/08/how-to-create-an-open-virtual-network-distributed-gateway-router/)
 * [CentOS deployment](https://computingforgeeks.com/configuring-open-vswitch-on-centos-rhel-fedora/)
 * [Use with libvirt](https://blog.scottlowe.org/2016/12/09/using-ovn-with-kvm-libvirt/)
 * [Proxmox Guide](https://pve.proxmox.com/wiki/Open_vSwitch)
