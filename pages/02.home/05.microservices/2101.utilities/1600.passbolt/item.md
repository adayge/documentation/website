---
title: Passbolt
date: March 2, 2020
taxonomy:
    category: [Microservices]
    tag: [microservice, virtualmachine]
    author: DaveLoper
---

Passbolt is an open source web platform for password storage and management. It is similar to Lastpass or Dashlane in features and functionality.

# Installation

## Platform Prerequisite

Recommended Platform:
  * Proxmox
  * Docker

Prerequisite Container:
  * Mariadb/MySQL

Optional:
  * NGINX reverse proxy for easy security and stacking

## Deployment

# Support
If your password creation is too slow, you may want to increase the speed on your random number generator by [using this guide](../../06.platforms/1802.rng/default.md).

## Backup

## Restore


# Links
 * [This page discussion on Adayge Forums](https://forums.adayge.com/t/passbolt-an-alternative-to-lastpass-dashlane/33)
 * [Passbolt's website](https://www.passbolt.com/)
 * [Passbolt on docker hub](https://hub.docker.com/r/passbolt/passbolt/)
