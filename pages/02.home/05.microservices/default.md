---
title: Microservices
visible: false
---

# Microservices
adagYge supports both its own microservices and the microservices listed in this catalog.

## Business applications
**OpenMAINT** - adaYge supports OpenMAINT as a microservice. This powerful web-based application allows for your business to track, service, and maintain your business assets. Everything from buildings to cabinets, fleet vehicles, PCs, servers, and fire extinguishers. OpenMAINT can help you maintain all your stuff.

## File and Storage
**IDEArchive (Alpha/BETA)** - adaYge creates microservices which monitor, provision, and publish secure storage that conserves space through thin provisioning, data deduplication, and secure communications. In addition to giving you access to your data no matter where you may be, IDEArchive will preserve backup copies of your data making it effective for backup and recovery. Moreover, this backup is immune to cryptolockers and viruses even if your machine gets infected.

## Management and Infrastructure
**Portainer** - adaYge supports container management through managed Portainer container services. This module allows you to easily provision and manage your microservices.

**certifYcate (coming soon)** - this service helps you acquire, provision, renew, revoke, and distribute SSL certificates throughout your organization. The certifYcate service securely communicates with your DNS provider. It leverages free certificate services from Let's Encrypte or your desired certificate provider. It lets you know when your certs are near expiry and also lets you know when it succeeds or fails are auto-renewing and auto-distributing new certificates.

## Video Conferencing
**Jitsi Meet** - adaYge combines Jitsi's own container service with certificate renewal services, dynamic clustered services, monitoring and reporting, and managed services to ensure that your video conferencing services are always available.
 * Setting up your own Jitsi Meet server
 * Get Managed Video Conferencing services from adaYge
