---
title: Proxmox
date: December 20, 2020
taxonomy:
    category: [Platforms]
    tag: []
    author: DaveLoper
---

Proxmox focuses on virtualization and container management through QEMU virtual machines and LXC containers.

# Installation
Download the ISO and commit it to USB.

# Management
Management of Proxmox is primarily achieved through a web interface.

## Updates

If you are using the community version of proxmox, you will need to switch to the community updates.

```
mkdir -p /etc/apt/sources.list.d/backup
mv /etc/apt/sources.list.d/pve-enterprise.list /etc/apt/sources.list.d/backup/
cat > /etc/apt/sources.list.d/pve-community.list << 'EOF'

deb http://ftp.debian.org/debian buster main contrib
deb http://ftp.debian.org/debian buster-updates main contrib

# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve buster pve-no-subscription

# securityupdates
deb http://security.debian.org/debian-security buster/updates main contrib
EOF

apt update
apt -y upgrade
```

It is recommended that after you update that you reboot the system in order for new kernels to take effect.

## Packages and Options
Several packages are useful to Proxmox for utility within Proxmox or at command line.

  * **ifupdown2**
    * This package is needed in order for the 'apply' button within the interface to function. It is unclear why this utility is not automatically installed.
  * **net-tools**
    * These tools allow you to do things like 'netstat'.
  * **screen**
    * Allows for screen sharing
  * **vim**
    * If you use vi, you will want this upgrade.

  * ****

```
apt -y install ifupdown2 net-tools screen vim
```

If you plan on running Docker on Proxmox you will need to follow the specific guide for [Proxmox here](../0402.docker/default.md).

# Microservices
Common microservices compatibility list

# Links
 * [PVE Community Updates](https://pve.proxmox.com/wiki/Package_Repositories#sysadmin_no_subscription_repo)
