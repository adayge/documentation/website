---
title: CentOS
date: December 13, 2020
taxonomy:
    category: [Platforms]
    tag: [platform, centos, podman]
    author: DaveLoper
---

# CentOS
CentOS is a Linux distribution which is based off of Redhat Enterprise Linux (RHEL). Redhat releases its open source code in compliance with the various open source licenses that comprises its solution. The CentOS community has traditionally taken these source code packages and recompiled them, turns them into packages and distributes it as a free operating system. In the future, CentOS will use the latest packages under development and become the effective 'beta' for RHEL.

CentOS does not have professional support services but rather is useful as a way to demonstrate or test applications and services for which you wish to run RHEL.

# Installation
Setting up CentOS for microservices is easy to do, the following instructions will help you setup your server for adaYge microservices.

# Management
By default, CentOS includes Cockpit for management. It is recommended that you  use podman instead of docker in most cases where you would see instructions for docker. In fact, if you see a guide here that has the command 'docker', type 'podman' instead for all instances of CentOS and RHEL.

Portainer is not yet fully compatible with podman. Use Cockpit instead to manage your containers from a GUI environment.

# Microservices
Common microservices compatibility list

# Links
 * [centos.org]
