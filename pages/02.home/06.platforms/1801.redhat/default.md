---
title: Redhat
date: March 2, 2020
taxonomy:
    category: [Platforms]
    tag: []
    author: DaveLoper
---

Redhat Enterprise Linux (RHEL) is a popular Linux distribution originating from North America intended for enterprise and professional environments. While the source code is included is provided publicly, compiled packages are licensed and protected.

# Installation
RHEL is heavily documented online. The current versions are [RHEL 7](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/index) and [RHEL 8](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/performing_a_standard_rhel_installation/index).

# Management
RHEL includes a graphical management platform called cockpit.

# Microservices
RHEL uses and supports containers using podman. While it is possible to run docker, Redhat's official support is geared towards podman having dropped support for docker.

# Links
  * [Redhat's Website](https://www.redhat.com/en)
  * [Redhat 7 Installation Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/index)
  * [Redhat 8 Installation Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/performing_a_standard_rhel_installation/index)
