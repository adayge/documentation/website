---
title: ClearOS
date: December 24, 2020
taxonomy:
    category: [Platforms]
    tag: [platform, clearos, podman]
    author: DaveLoper
---

# ClearOS
ClearOS is a Linux distribution which is based off of CentOS. CentOS releases its open source code and binaries with the various open source licenses that comprises its solution. ClearOS takes many of these packages and combines them with a framework and management interface for easy use with a focus on small business and distributed enterprise deployments.

ClearOS has 3 versions, a community version, a home version, and a business version. ClearOS has professional support services. Adayge also provides support for ClearOS systems.

# Installation
Setting up CentOS for microservices is easy to do, the following instructions will help you setup your server for adaYge microservices. ClearOS runs well on hardware and also as a virtual machine under any of the platforms supported by Adayge.

# Management
By default, CentOS includes Webconfig for management. It is recommended that you  use podman instead of docker in most cases where you would see instructions for docker. In fact, if you see a guide here that has the command 'docker', type 'podman' instead for all instances of ClearOS, CentOS, and RHEL.

Portainer is not yet fully compatible with podman. Use Cockpit instead to manage your containers from a GUI environment. Take care not to use cockpit for managing network resources as it can cause problems with ClearOS. Use ClearOS' webconfig instead.

# Microservices
ClearOS runs integrated apps from a management interface. You can mix and match services that are supported from Adayge as you see fit:

|Service                       |ClearOS|Adayge and other Platforms|
|------------------------------|-------|--------------------------|
|Antimalware Updates           |   X   ||
|Antispam Rules                |   X   ||
|Content Filter Blacklists     |   X   ||
|DNSthingy/AdamONE             |   X   ||
|Dynamic DNS                   |   X   ||
|Dynamic VPN                   |   X   ||
|Google Apps Account Sync      |   X   ||
|IDS Signatures                |   X   ||
|Remote Data Backup            |   X   ||
|Remote Security Audit         |   X   ||
|Remote System Monitor         |   X   ||
|Software Updates              |   X   ||
|1-to-1 NAT                    |   X   ||
|Bandwidth Manager             |   X   ||
|Bandwidth and QoS             |   X   ||
|Custom Firewall               |   X   ||
|DHCP Server                   |   X   ||
|DMZ                           |   X   ||
|DNS Server                    |   X   ||
|Dynamic Firewall              |   X   ||
|Egress Firewall               |   X   ||
|Firewall                      |   X   ||
|IP Settings                   |   X   ||
|ibVPN                         |   X   ||
|MultiWAN                      |   X   ||
|NTP Server                    |   X   ||
|Network Map                   |   X   ||
|OpenVPN                       |   X   ||
|PPTP Server                   |   X   ||
|Port Forwarding               |   X   ||
|RADIUS Server                 |   X   ||
|SSH Server                    |   X   ||
|IPSec VPN                     |   X   ||
|UPnP                          |   X   ||
|Application Filter            |   X   ||
|Attack Detector               |   X   ||
|Content Filter Engine         |   X   ||
|Gateway Antiphishing          |   X   ||
|Gateway Antivirus             |   X   ||
|Gateway Management            |   X   ||
|Intrusion Detection System    |   X   ||
|Intrusion Prevention System   |   X   ||
|Kaspersky Antimalware         |   X   ||
|Protocol Filter               |   X   ||
|Web Access Control            |   X   ||
|Web Proxy Auto Discovery      |   X   ||
|Web Proxy Server              |   X   ||
|AMIBIOS                       |   X   ||
|Advanced Print Server         |   X   ||
|Antimalware File Scanner      |   X   ||
|ClearGlass                    |   X   ||
|Directory Server              |   X   ||
|Domoticz Home Automation      |   X   ||
|FTP Server                    |   X   ||
|Flexshare                     |   X   ||
|iLO                           |   X   ||
|IMAP and POP Server           |   X   ||
|Kopano                        |   X   ||
|Mail Antispam                 |   X   ||
|Mail Antivirus                |   X   ||
|Mail Archive                  |   X   ||
|Mail Greylisting              |   X   ||
|Mail Retrieval                |   X   ||
|MariaDB Database Server       |   X   ||
|Microsoft AD Connector        |   X   ||
|Nextcloud                     |   X   ||
|ownCloud                      |   X   ||
|PHP Engines                   |   X   ||
|Photo Organizer               |   X   ||
|Plex Media Server             |   X   ||
|Kaspersky Mail Antivirus      |   X   ||
|ProxyPass                     |   X   ||
|Roundcube                     |   X   ||
|SMTP Server                   |   X   ||
|Serviio Media Server          |   X   ||
|Transmission BitTorrent Client|   X   ||
|Web Server                    |   X   ||
|Windows Networking(Samba)     |   X   ||
|Wordpress                     |   X   ||
|Zarafa                        |   X   ||
|2FA for Webconfig             |   X   ||
|APC Battery Backup            |   X   ||
|Account Import                |   X   ||
|Account Manager               |   X   ||
|Account Import                |   X   ||
|Account Synchronization       |   X   ||
|Administrators                |   X   ||
|BackupPC                      |   X   ||
|Bare Metal Backup and Restore |   X   ||
|Dashboard                     |   X   ||
|Date and Time                 |   X   ||
|Groups                        |   X   ||
|Let's Encrypt                 |   X   ||
|Mail Notification             |   X   ||
|Mail Settings                 |   X   ||
|Marketplace                   |   X   ||
|Master/Slave Synchronization  |   X   ||
|Password Policies             |   X   ||
|Services                      |   X   ||
|Shell Extension               |   X   ||
|Software RAID Manager         |   X   ||
|Storage Manager               |   X   ||
|Support                       |   X   ||
|System Registration           |   X   ||
|User Certificates             |   X   ||
|User Profile                  |   X   ||
|Users                         |   X   ||
|Webconfig                     |   X   ||
|Bandwidth Viewer              |   X   ||
|Disk Usage Report             |   X   ||
|Events and Notifications      |   X   ||
|Filter and Proxy Report       |   X   ||
|Log Viewer                    |   X   ||
|Network Report                |   X   ||
|Network Visualizer            |   X   ||
|Process Viewer                |   X   ||
|Resource Report               |   X   ||
|Smart Monitor                 |   X   ||
|System Report                 |   X   ||


# Links
 * [ClearOS Website](https://clearos.com)
