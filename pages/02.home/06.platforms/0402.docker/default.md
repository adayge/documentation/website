---
title: Docker
date: December 20, 2020
taxonomy:
    category: [Platforms]
    tag: []
    author: DaveLoper
---

# Docker
Docker is a container platform. It runs application in security segmented user space. This increased security coupled with powerful tools that allow for portability make containers a powerful mechanism for running modern applications.

# Installation
Docker is supported on a variety of platforms including:

 * Debian
 * Ubuntu
 * Proxmox

**NOTE:** For Fedora-based distributions, use Podman instead.

## Proxmox
Docker is not native on Proxmox nor is it supported upstream from Proxmox support. Adayge, however, supports this configuration. In addition to docker, many services from Adayge also use docker-compose. We will install that this as well.

```
apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

After the prerequisites are installed, download the keys and install docker:

```
curl -fsSL https://download.docker.com/linux/debian/gpg > /root/docker.gpg
cat /root/docker.gpg | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get update && apt-get install docker-ce -y
```

To validate your install, run the following:
```
docker run hello-world
```


# Microservices
Common microservices compatibility list

# Links
