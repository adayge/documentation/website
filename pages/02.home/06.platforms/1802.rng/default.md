---
title: Random Number Generators
date: March 2, 2021
taxonomy:
    category: [Platforms]
    tag: []
    author: DaveLoper
---

Certain applications and services rely on random number generation in order to function. These include applications like VPN, password generation, and other security-centric applications.

By default, Linux creates devices like /dev/random and /dev/urandom which can be used by programs and are the standard for random number generation. These rely on the entropy available to them to create their results. For purposes of performance and security, /dev/urandom is faster and less random, while /dev/random is slower and considered more random. This has more to do with the fact that /dev/urandom can be asked to generate a contiguous result of a size you specify. For example:

```
dd if=/dev/urandom of=~/my1MB.random bs=1M count=1
```

To determine how much randomness your computer can generate, you can pull the current statistic for this by running:

```
cat /proc/sys/kernel/random/entropy_avail
```

A result of less than 1000 is considered too slow for most applications. The typical result of having too slow of a random number generator on Linux is that the read action against your random device will stall in the same way that a read operation on a disk does resulting in a block processes until it is read to the length required.

These devices are commonly called psuedo-random generators (PRNG) since they source their randomness from devices on the system which are electro-mechanical. Usually, this is sufficient for most security applications.

Additional devices, such as true random number generator devices (TRNG) rely on quantum-mechanical events to create the entropy needed. Generally these are optimized for faster output than the built-in PRNG devices.

If your system has a TRNG device installed, you can link it to your built-in device using 'rng-tools' and its accompanying daemon service. This will augment your /dev/random and /dev/urandom with the entropy of the TRNG causing it to also become a TRNG source. No extra work is required to get your applications and services to function with the TRNG.

If your system requires increased entropy because the result of `cat /proc/sys/kernel/random/entropy_avail` is too low, you can use another tool called 'haveged' which is a software/daemon tool that applies the [HAVEGE algorithm](https://www.irisa.fr/caps/projects/hipsor/misc.php) in order to produce effective unpredictability.

The rest of this guide will help you set up rng-tools or haveged depending on your hardware. We do not recommend rng-tools unless you have a hardware-based TRNG device.

# rng-tools
The rng-tools package allows for a hardware device to pump its random numbers into /dev/urandom. This is the most enterprise approach to applying additional random numbers in a way that overcomes the psuedo part of the built-in PRNG of /dev/urandom.

Typical TRNG and Quantum Random Number Generator (QRNG) devices cost several hundred or several thousands of dollars each.

## Installation

**Debian and Ubuntu**
```
apt -y install rng-tools
systemctl enable rng-tools
systemctl start rng-tools
```

**Fedora, CentOS, RHEL**
```
yum -y install rng-tools
systemctl enable rngd
systemctl start rngd
```



## Management
Once installed, you can get the status, start, enable, stop, or disable the daemon with the following commands respectively.

**Debian and Ubuntu**
```
systemctl status rng-tools
systemctl start rng-tools
systemctl enable rng-tools
systemctl stop rng-tools
systemctl disable rng-tools
```

In Debian/Ubuntu your device can be configured from the **/etc/default/rng-tools** configuration file.

**Fedora, CentOS, RHEL**
```
systemctl status rngd
systemctl start rngd
systemctl enable rngd
systemctl stop rngd
systemctl disable rngd
```

Please note that by default in RHEL/CentOS, this process will use the device **/dev/hwrandom** for its hardware device. You set up your hardware to use this device or your can change the destination by modifying the systemctl daemon at **/usr/lib/systemd/system/rngd.service**. The lack of a built-in configuration directive for the CentOS/RHEL package demonstrates that this package is not modernized and may be poorly maintained. While modification of the service file will result in a functional RNG device at a different location, the service file is part of the package and can be overwritten by future updates. Update this package carefully if you modify the service file in any way (even if it is to include a configuration file).

A good test is to validate your entropy with the daemon running versus the daemon stopped.

> NOTE: guides that instruct you to use the /dev/urandom device as the entropy device are flawed in their recommendation. This project is designed to work with hardware devices. Pointing this daemon to the /dev/urandom can lead to decreased randomness over time. This is because you've just created a loopback by having /dev/urandom seed /dev/urandom

# haveged
The **haveged** daemon service is a great tool for providing a software-based solution to improve your RNG performance.

By default, haveged will fill up the the /dev/random device with extra random bits if the /dev/random device has a shortfall.

**Debian and Ubuntu**
```
apt -y install haveged
systemctl enable haveged
systemctl start haveged
```

**Fedora, CentOS, RHEL**
> NOTE: you will need to have the EPEL repositories installed and configured to use **haveged**

```
yum -y install haveged
systemctl enable haveged
systemctl start haveged
```

## Management
Once installed, you can get the status, start, enable, stop, or disable the daemon with the following commands respectively.

**Debian and Ubuntu**
```
systemctl status haveged
systemctl start haveged
systemctl enable haveged
systemctl stop haveged
systemctl disable haveged
```

In Debian/Ubuntu your daemon service can be configured from the **/etc/default/haveged** configuration file.

**Fedora, CentOS, RHEL**
```
systemctl status haveged
systemctl start haveged
systemctl enable haveged
systemctl stop haveged
systemctl disable haveged
```

As with rng-tools, RHEL does not have good configuration support for haveged. The lack of a built-in configuration directive for the CentOS/RHEL package demonstrates that this package is not modernized and may be poorly maintained. While modification of the service file will result in a functional RNG device at a different location, the service file is part of the package and can be overwritten by future updates. Update this package carefully if you modify the service file in any way (even if it is to include a configuration file). The config file installed by RHEL/CentOS is here:

```
/usr/lib/systemd/system/haveged-switch-root.service
```

A good test is to validate your entropy with the daemon running versus the daemon stopped.

# Links
  * [Great article on ArchLinux](https://wiki.archlinux.org/index.php/Rng-tools)
