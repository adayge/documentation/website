---
title: 'Platforms Overview'
date: '13-12-2020 00:00'
taxonomy:
    category:
        - Platforms
    tag:
        - platform
    author:
        - DaveLoper
visible: false
---

# Platforms Overview
Adayge supports various platforms which are used for microservices from 3rd parties and Adayge's own applications. Currently these platforms are (in alphabetical order):

 * [CentOS](centos)
 * [Debian](debian)
 * [Proxmox](proxmox)
 * [Redhat Enterprise Linux](redhat)
 * [Ubuntu](ubuntu)

Adayge is happy to consider additional platforms. If you are a contributor or customer that needs platform support on a different platform, please contact us for requirements.

Within these platforms, additional microservices platforms and management portals are also supported, including:

 * [Cockpit](cockpit)
 * [Docker](docker)
 * [LXC](lxc)
 * [Podman](podman)
 * [Portainer](portainer)


## We're Here to Help
Professionals at Adayge are here to help you get started with your microservice capable platform. We can help you configure your platform in high availability configurations, scalable compute environments, private and public cloud, and hyper-converged environments. You can use our handy guides on this site for free, hire us to train you or your staff on advanced engineering and concepts, or have us or one of our partners deploy and manage your services. Anyway you go, we're here to help.
