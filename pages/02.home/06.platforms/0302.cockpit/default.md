---
title: Cockpit
date: December 13, 2020
taxonomy:
    category: [Platforms]
    tag: [platform]
    author: DaveLoper
---

# Platform Name
Cockpit is a multi-OS management platform that is useful for system management and maintenance from a useful web-based UI.

# Installation
Instructions

# Microservices
Common microservices compatibility list

# Links
 * [Official Cockpit Website](https://cockpit-project.org/)
