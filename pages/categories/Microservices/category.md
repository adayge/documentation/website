---
title: Microservices
category: Microservices
---

Microservices are the most impactful aspect to adaYge services. Carefully tied together they can provide a robust and resilient IT architecture. Think of the functionality of Microservices like you would applications. Think of the way you run microservices like a redundant power grid. Think of the convenience, like plugging in a toaster.

To run your first microservice you will need the infrastructure to get it running. The platform section can help you get the infrastructure that you need. We can help you get that setup or can help you manage it through training, partners, and support.

Our library of microservices are always expanding. Your feedback is vital to which services are in the most demand and what our focus should be. We encourage participation in our forums and contact with us directly.
